﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float jump_speed;
    [SerializeField] float rotate_speed;
    private float graity_force = 9.81f;
    Vector3 player_move;
    private CharacterController ch_controller;
    public void Start()
    {
        ch_controller = GetComponent<CharacterController>();   
    }
    public void Update()
    {
        if(ch_controller.isGrounded)
        {
            player_move = Vector3.zero;
            player_move.x = Input.GetAxis("Horizontal") * speed;
            player_move.z = Input.GetAxisRaw("Vertical") * speed;
        }
        if(Vector3.Angle(Vector3.forward,player_move) > 1f || Vector3.Angle(Vector3.forward, player_move) == 0)
        {
            Vector3 direct = Vector3.RotateTowards(transform.forward, player_move, speed, 0.0f);
            transform.rotation = Quaternion.LookRotation(direct);
        }
        player_move.y = graity_force;
        ch_controller.Move(player_move * Time.deltaTime);
    }
    public void Gravity()
    {
        if(!ch_controller.isGrounded)
        {
            graity_force -= 20f * Time.deltaTime;
        }
        else
        {
            graity_force = -1f;
        }
        if (Input.GetKeyDown(KeyCode.Space) && ch_controller.isGrounded) graity_force = jump_speed;
    }
}
